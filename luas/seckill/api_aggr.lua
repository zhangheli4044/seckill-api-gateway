
local _M = {}

local cjson = require "cjson"
            
function _M.capture_multi(urls)
    -- construct the requests table
    local reqs = {}

    for i, url in ipairs(urls) do
        table.insert(reqs, { url.path, {
            copy_all_vars = true,
            args          = { url = url.args },
            method        = ngx.HTTP_GET,
        }})
    end

    -- issue all the requests at once and wait until they all return
    local resps = {
        ngx.location.capture_multi(reqs)
    }
    local results = {}
    -- loop over the responses table
    for i, resp in ipairs(resps) do
        table.insert(results, {status = resp.status, body = resp.body})
    end
    ngx.say(cjson.encode({results = results}))
end

return _M
