local goodsId = KEYS[1]
local now = tonumber(KEYS[2])
local uid = KEYS[3]
local orderId = KEYS[4]

local uidOrderKey = string.format("seckill:uid_orders:%s",goodsId)
local inventoryKey = string.format("seckill:inventory:%s",goodsId)

local vals = redis.call("hmget", inventoryKey, "total","booked","paid","startTime")
local total = tonumber(vals[1])
local booked = tonumber(vals[2])
local paid = tonumber(vals[3])
local startTime = tonumber(vals[4])



if startTime == nil or not startTime or startTime > now then
    return "295"
end

if paid >= total then
    return "296"
end

if booked >= total then
    return "297"
end

redis.call("HINCRBY", inventoryKey,"booked",1)
redis.call("hmset",uidOrderKey,uid,orderId)

return "000"