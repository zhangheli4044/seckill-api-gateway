
local redis = require "resty.redis"
local _M = {}           -- 局部的变量

local host = "redis.default.svc.cluster.local"
local port = 6379
local pwd = "Hangzhou@123"

-- get_redis_client
function _M.get_redis_client()
    local red = redis:new()
    red:set_timeouts(1000, 1000, 1000)
    local ok, err = red:connect(host,port)

    if not ok then
        return nil, err
    end
    local res, err = red:auth(pwd)
    if not res then
        return nil, err
    end
    return red,err
end


return _M
