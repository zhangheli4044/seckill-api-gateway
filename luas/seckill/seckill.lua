local _M = {}

local uuid = require 'resty.uuid'
local cjson = require "cjson"
local cjson2 = cjson.new()

local redis_client = require("redis_client")
local ngx = ngx
local seckill_sha = "afc14595bf973a5d617b6b32c0ce945d724a4c49"
local db = 0

local function getParams()
    local request_method = ngx.var.request_method
    local args = nil
    local uid = nil
    local goodsId = nil
    --获取参数的值
    if "GET" == request_method then
        args = ngx.req.get_uri_args()
    elseif "POST" == request_method then
        ngx.req.read_body()
        args = ngx.req.get_post_args()
    end
    uid = args["uid"]
    goodsId = args["goodsId"]
    return uid,goodsId
end


local function checkUid(uid,red)
    -- check uid not emply
    if uid == nil or uid == "" then
        return false,"uid not ok"
    end

    -- can check uid not in black list
    return true
end

local function seckillGoods(uid,goodsId,red)
    -- check uid not emply
    if not uid or not goodsId then
        return false,"params not ok"
    end

    -- check exsist order
    local uidOrderKey = string.format("seckill:uid_orders:%s",goodsId)
    local exsist_vals,err = red:hmget(uidOrderKey,uid)
    if exsist_vals ~= ngx.null then
        -- return exsist order
        local orderId = exsist_vals[1]
        if orderId ~= ngx.null and orderId ~= nil then
            return true,nil,orderId
        end
    end

    -- check inventory
    local orderId = string.gsub(uuid.generate(),"-","",4)
    local vals, err = red:evalsha(seckill_sha,4,goodsId,ngx.now() * 1000,uid,orderId)
    if vals ~= "000" then
        return false,vals,nil
    end

    -- push a order job to queue
    local order_queue_key = "seckill:orders_queue"
    local order_object = {
        ["goodsId"] = goodsId,
        ["orderId"] = orderId,
        ["uid"] = uid ,
        ["byCount"] = 1
    }
    red:lpush(order_queue_key,cjson2.encode(order_object))

    return true,nil,orderId
end


function _M.do_seckill()
    local uid,goodsId = getParams()

    local red,err = redis_client.get_redis_client()

    if red == nil then
        ngx.say(err)
        return
    end
    local ok, err = red:select(db)
    if not ok then
        ngx.say(err)
        return
    end

    local uid_ok,err = checkUid(uid,red)

    if not uid_ok then
        ngx.say(err)
        red:set_keepalive(10000, 100)
        return
    end

    local goods_ok,err,orderId = seckillGoods(uid,goodsId,red)

    if not goods_ok then
        ngx.say(err)
        red:set_keepalive(10000, 100)
        return
    end

    local rs = {
        ["code"] = "000",
        ["data"] = orderId
    }
    ngx.say(cjson2.encode(rs))

    red:set_keepalive(10000, 100)
end

return _M