local _M = {}

local jwt = require "resty.jwt"
local cjson = require "cjson"
local cjson2 = cjson.new()

local redis_client = require("redis_client")

local db = 3


local function get_sid()
    local raw_cookie = ngx.req.get_headers()["Cookie"]
    if raw_cookie == nil then
        return nil
    else
    -- string.gfind is renamed to string.gmatch
      for item in string.gmatch(raw_cookie, "[^;]+") do
        local _, _, k, v = string.find(item, "^%s*(%S+)%s*=%s*(%S+)%s*")
        if k ~= nil and k == "_fotor_sid" and v~= nil then
          return v
        end
      end
    return nil
    end
end


function get_token()
    local auth_header = ngx.var.http_Authorization
    if auth_header == nil then
        return nil
    end
    -- require Bearer token
    local _, _, token = string.find(auth_header, "bearer%s+(.+)")
    return token
end
  

  
  -- 在redis token中取传入的sid, 不为空则为登录用户
function get_value_from_redis(key)
    local red, err = redis_client.get_redis_client()
    if not red then
      return false, nil, err
    end
    local ok, err = red:select(db)
    if not ok then
      return false,nil, err
    end
    local rdsValue, err = red:get(key)
    if err then
      return false, nil, err
    end
  
    red:set_keepalive(10000, 100)
    if rdsValue == ngx.null then
      return false, nil, nil
    end
  
    local obj = cjson2.decode(rdsValue)
    return true, obj, nil
end


function _M.validation()
  local token = get_token()
  local tokenType = "oauth"
  if token == nil then
      token = get_sid()
      tokenType = "token"
  end

  local ok, obj, err = get_value_from_redis(string.format("%s:%s", tokenType,token))

  if ok and nil ~= obj and obj.uid and obj.uid ~= nil then
      ngx.req.set_header("uid", obj.uid)
  end
end


function _M.test()
  local jwt_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" ..
                    ".eyJmb28iOiJiYXIifQ" ..
                    ".VAoRL1IU0nOguxURF2ZcKR0SGKE1gCbqwyh8u2MLAyY"
  local jwt_obj = jwt:verify("lua-resty-jwt", jwt_token)
  ngx.say(cjson.encode(jwt_obj))
end

return _M
